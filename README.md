
## Moved ##

As of commit 83213fe00d767e4723e61910d54c431b952865ac, this repository was moved into
the [thornode](https://gitlab.com/thorchain/thornode) repository, under [test/smoke/](https://gitlab.com/thorchain/thornode/-/tree/develop/test/smoke)

See https://gitlab.com/thorchain/thornode/-/merge_requests/2410 for details of the merge.

Please open any new PRs against the thornode repository instead of here.